using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadows : MonoBehaviour
{
    public static Shadows me;
    public GameObject shadow;
    public List<GameObject> pool = new List<GameObject>();
    private float timer;
    public float speed;
    public Color myColor;

    private void Awake()
    {
        me = this;
    }

    public GameObject GetShadows()
    {
        for(int i = 0; i <pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                pool[i].SetActive(true);
                pool[i].transform.position = transform.position;
                pool[i].transform.rotation = transform.rotation;
                pool[i].GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
                pool[i].GetComponent<Solid>().myColor = myColor;
                return pool[i];
            }

        }
        GameObject obj = Instantiate(shadow, transform.position, transform.rotation) as GameObject;
        obj.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
        obj.GetComponent<Solid>().myColor = myColor;
        pool.Add(obj);
        return obj;
    }

    public void ShadowsKill()
    {
        timer += speed * Time.deltaTime;
        if(timer > 1)
        {
            GetShadows();
            timer = 0;
        }
    }

}
