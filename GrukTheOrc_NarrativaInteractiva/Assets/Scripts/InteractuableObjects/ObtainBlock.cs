using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObtainBlock : MonoBehaviour
{
    public GameObject blockItem;
    void Update()
    {
        if (GameObject.Find("Personaje").GetComponent<Player>().ability == 1)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            blockItem.SetActive(true);
            collision.gameObject.GetComponent<Player>().ability = 2;
        }
    }
}
