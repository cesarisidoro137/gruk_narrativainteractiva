using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyLife : MonoBehaviour
{
    [SerializeField]
    private int cost;
    private int playerMoney;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    void Update()
    {
        playerMoney = GameObject.Find("Personaje").GetComponent<Player>().quantityofCoins;
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && playerMoney >= cost)
        {
            collision.gameObject.GetComponent<Player>().GainMaxLife(cost);
            Destroy(gameObject);
        }
        
    }
}
