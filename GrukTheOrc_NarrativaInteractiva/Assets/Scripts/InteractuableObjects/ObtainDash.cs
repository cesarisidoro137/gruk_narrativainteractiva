using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObtainDash : MonoBehaviour
{
    public GameObject Itemdash;

    private void Update()
    {
        if (GameObject.Find("Personaje").GetComponent<Player>().ability == 2)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Itemdash.SetActive(true);
            collision.gameObject.GetComponent<Player>().ability = 1;
        }
    }
}
