using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicBackground3 : MonoBehaviour
{
    public static MusicBackground3 instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (!(SceneManager.GetActiveScene().name == "RutaCastillo_C" || SceneManager.GetActiveScene().name == "FinalRutaCastillo_C"))
        {
            Destroy(gameObject);
        }
    }
}
