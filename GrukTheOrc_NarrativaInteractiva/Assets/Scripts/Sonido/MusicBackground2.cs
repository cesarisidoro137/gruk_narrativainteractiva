using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicBackground2 : MonoBehaviour
{
    public static MusicBackground2 instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (!(SceneManager.GetActiveScene().name == "RutaOrgullo_C" || SceneManager.GetActiveScene().name == "FinalRutaOrgullo_C"))
        {
            Destroy(gameObject);
        }
    }
}
