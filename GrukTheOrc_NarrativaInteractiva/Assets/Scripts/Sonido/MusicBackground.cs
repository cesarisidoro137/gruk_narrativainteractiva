using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicBackground : MonoBehaviour
{
    public static MusicBackground instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (!(SceneManager.GetActiveScene().name == "Main menu" || SceneManager.GetActiveScene().name == "Creditos"))
        {
            Destroy(gameObject);
        }
    }
}
