using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicBackground1 : MonoBehaviour
{
    public static MusicBackground1 instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (!(SceneManager.GetActiveScene().name == "RutaVerdaderoRutaCueva_C" || SceneManager.GetActiveScene().name == "FinalVerdaderoCueva_C"))
        {
            Destroy(gameObject);
        }
    }
}
