using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigerSound : MonoBehaviour
{
    public AudioClip clip1;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            BgmController.bgm.changeaudio(clip1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Destroy(gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        CheckGround.isGrounded = false;
    }
}
