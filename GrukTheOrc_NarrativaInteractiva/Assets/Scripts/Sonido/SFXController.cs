using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    public List<SFX> sfx; // Lista de efectos de sonido definidos en el inspector.
    static SFXController sfxController; // Referencia est?tica al controlador de efectos de sonido.
    public bool canUpdateVolume; // Indicador de si se puede actualizar el volumen.

    public void Awake()
    {
        // Configuraci?n para asegurarse de que solo haya un controlador de efectos de sonido en el juego.
        if (sfxController == null)
        {
            sfxController = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
        canUpdateVolume = true;
    }

    public static SFXController GetSFX()
    {
        return sfxController;
    }

    public void PlaySound(string name, AudioSource audioSource)
    {
        AudioClip sound = null;

        // Buscar el efecto de sonido en la lista por su nombre.
        foreach (var s in sfx)
        {
            if (s.name == name)
            {
                sound = s.audioclip;
            }
        }
        // Si se encuentra el efecto de sonido, reproducirlo en el AudioSource proporcionado.
        if (sound != null)
        {
            audioSource.PlayOneShot(sound);
        }
    }
}
