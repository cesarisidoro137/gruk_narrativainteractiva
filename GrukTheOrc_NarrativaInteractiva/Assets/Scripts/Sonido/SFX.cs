using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class SFX
{
    public string name; // El nombre del efecto de sonido.
    public AudioClip audioclip; // El archivo de audio del efecto de sonido.
}
