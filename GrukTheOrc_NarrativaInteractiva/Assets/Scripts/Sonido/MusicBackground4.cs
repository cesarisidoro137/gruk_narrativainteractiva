using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicBackground4 : MonoBehaviour
{
    public static MusicBackground4 instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (!(SceneManager.GetActiveScene().name == "RutaOrgullo_C" || SceneManager.GetActiveScene().name == "FinalRutaOrgullo_C"))
        {
            Destroy(gameObject);
        }
    }
}
