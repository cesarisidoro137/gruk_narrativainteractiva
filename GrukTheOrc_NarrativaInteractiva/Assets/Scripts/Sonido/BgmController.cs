using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgmController : MonoBehaviour
{
    public AudioSource auM;
    public static BgmController bgm;

    private void Start()
    {
        bgm = this;
    }
    public void changeaudio(AudioClip clip)
    {
        auM.clip = clip;
        auM.Play();
    }
}
