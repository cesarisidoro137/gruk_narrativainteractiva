using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallestaHazard : MonoBehaviour
{
    public Transform player;
    public Transform shootPoint;
    public float rotationSpeed;
    public GameObject arrowPrefab;

    public float fireRate; // Tiempo entre disparos en segundos
    private float nextFireTime; // Tiempo del pr�ximo disparo
    public float detectionRange = 10f; // Distancia a la cual detectar� al jugador

    public float force;

    private Animator animator;
    public AudioSource au;
    void Start()
    {
        animator = GetComponent<Animator>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        if (shootPoint == null)
        {
            Debug.LogError("ShootPoint not assigned for BallestaHazard in GameObject: " + gameObject.name);
        }
    }

    void Update()
    {
        if (player != null && Vector2.Distance(transform.position, player.position) <= detectionRange)
        {
            RotateTowardsPlayer();
            //animator.Play("Ballesta");
            if (Time.time > nextFireTime)
            {
                nextFireTime = Time.time + 1 / fireRate;
                animator.SetBool("isAttack", true);
                //ShootArrow();
                //animator.SetBool("isAttack", true);
                //animator.SetBool("isAttack");
            }

        }
    }

    void RotateTowardsPlayer()
    {

        Vector2 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle - 0f, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }

    public void ShootArrow()
    {
        if (arrowPrefab != null && shootPoint != null)
        {
            SFXController.GetSFX().PlaySound("Ballesta", au);


            // Instantiate(arrowPrefab, shootPoint.position, transform.rotation);
            // Aqu� puedes agregar c�digo adicional para controlar la l�gica de disparo del proyectil

            // Calcula la rotaci�n necesaria para apuntar hacia la posici�n del jugador
            Vector2 direction = player.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            // Instancia la flecha con la rotaci�n calculada
            GameObject arrowInstance = Instantiate(arrowPrefab, shootPoint.position, rotation);
            arrowInstance.GetComponent<Rigidbody2D>().AddForce(direction.normalized * force * 10f);

        }
    }
    public void DesactivateAnim()
    {
        animator.SetBool("isAttack", false);
    }
}
