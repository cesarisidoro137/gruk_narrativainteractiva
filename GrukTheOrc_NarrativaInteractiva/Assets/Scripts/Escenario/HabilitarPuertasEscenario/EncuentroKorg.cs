using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncuentroKorg : MonoBehaviour
{
    public GameObject enemigo1;
    public GameObject enemigo2;

    public GameObject puertaEntrada;
   // public GameObject puertaSalida;

    public GameObject habilidad1;
    public GameObject habilidad2;
    private void Start()
    {
        // Activar ambas puertas al inicio del juego
        ActivarPuertas();
    }

    private void ActivarPuertas()
    {
        if (puertaEntrada != null)
        {
            puertaEntrada.SetActive(true);
        }

        /*if (puertaSalida != null)
        {
            puertaSalida.SetActive(true);
        }*/
    }

    private void DesactivarPuerta(GameObject puerta)
    {
        if (puerta != null)
        {
            puerta.SetActive(false);
        }
    }

    private void Update()
    {
        if (!habilidad1 || !habilidad2)
        {
           // DesactivarPuerta(puertaSalida);
        }
        if (!enemigo1 && !enemigo2)
        {
            DesactivarPuerta(puertaEntrada);
        }
    }
}
