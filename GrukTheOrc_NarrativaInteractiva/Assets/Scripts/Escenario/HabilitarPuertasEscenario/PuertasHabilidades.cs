using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertasHabilidades : MonoBehaviour
{
    public GameObject habilidad1;
    public GameObject habilidad2;

    public GameObject puerta1;
    public GameObject puerta2;

    private void Start()
    {
        // Activar ambas puertas al inicio del juego
        ActivarPuertas();
    }

    private void ActivarPuertas()
    {
        if (puerta1 != null)
        {
            puerta1.SetActive(true);
        }

        if (puerta2 != null)
        {
            puerta2.SetActive(true);
        }
    }

    private void DesactivarPuerta(GameObject puerta)
    {
        if (puerta != null)
        {
            puerta.SetActive(false);
        }
    }

    private void Update()
    {
        if(!habilidad1)
        {
            DesactivarPuerta(puerta2);
        }
        if (!habilidad2)
        {
            DesactivarPuerta(puerta1);
        }
    }
}
