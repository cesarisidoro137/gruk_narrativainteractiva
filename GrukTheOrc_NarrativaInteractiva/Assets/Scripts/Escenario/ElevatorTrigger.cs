using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorTrigger : MonoBehaviour
{
    public static bool isTrigger;

    private void Start()
    {
        isTrigger = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isTrigger= true;
        Destroy(this.gameObject);
    }
}
