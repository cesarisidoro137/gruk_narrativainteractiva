using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinchos : MonoBehaviour
{
    public float upTime = 2.0f;          // Time it takes to go up and down
    public float speed = 5.0f;           // Up and down speed
    public int damage = 10;              // Damage dealt to the player
  //  public Transform upperLimit;         // Transform marking the upper limit
   // public Transform lowerLimit;         // Transform marking the lower limit

    private bool goingUp = false;

    private void Start()
    {
        //StartCoroutine(SpikeCycle());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !goingUp)
        {
            other.gameObject.GetComponent<Player>().Recievedmg(damage);
            // Apply damage to the player
            // You can call a function in the player script to handle damage.
            // Example: other.GetComponent<PlayerHealth>().ReceiveDamage(damage);
        }
    }

  /*  private IEnumerator SpikeCycle()
    {
        while (true)
        {
            yield return StartCoroutine(MoveToLimit(upperLimit));
            yield return StartCoroutine(MoveToLimit(lowerLimit));
        }
    }*/

   /* private IEnumerator MoveToLimit(Transform targetLimit)
    {
        Vector3 targetPosition = targetLimit.position;
        float elapsed = 0f;
        Vector3 initialPosition = transform.position;

        while (elapsed < upTime)
        {
            transform.position = Vector3.Lerp(initialPosition, targetPosition, elapsed / upTime);
            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPosition;
    }*/
}
