using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    //SI O SI LA PALTAFORMA DEBE TENER DE ESCALA: 1,1,1
    public Transform[] puntosDeDestino;
    public float speed;
    private int indiceDestino = 0;

    private void Update()
    {
       MoverPlataforma();
    }

     private void MoverPlataforma()
      {
          if (puntosDeDestino.Length == 0)
          {
              Debug.LogError("A�ade al menos un punto de destino en el arreglo.");
              return;
          }

          Transform destino = puntosDeDestino[indiceDestino];
          transform.position = Vector2.MoveTowards(transform.position, destino.position, speed * Time.deltaTime);

          // Comprueba si la plataforma lleg� al destino
          if (Vector2.Distance(transform.position, destino.position) < 0.1f)
          {
              // Cambia al siguiente destino en el arreglo circularmente
              indiceDestino = (indiceDestino + 1) % puntosDeDestino.Length;
          }
      }
    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.transform.SetParent(this.transform);
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.transform.SetParent(null);
        }
    }
}
