using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public GameObject arrivePoint;
    public float speed;
    // Start is called before the first frame update

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ElevatorTrigger.isTrigger)
        {
            transform.position = Vector3.MoveTowards(transform.position, arrivePoint.transform.position, speed * Time.deltaTime);
        }
        if (Vector2.Distance(transform.position,arrivePoint.transform.position) <= 0.5)
        {
            
        }
    }
}
