using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretPaths : MonoBehaviour
{
    SpriteRenderer sprite;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            sprite.color = new Color(0, 0, 0, 0.5f);
            // el 0.5f cambia la Opacidad del objeto
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            sprite.color = Color.white; // cambia a su color original
        }
    }
}
