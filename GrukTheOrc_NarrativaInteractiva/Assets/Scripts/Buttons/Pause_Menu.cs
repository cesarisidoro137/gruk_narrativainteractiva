using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Pause_Menu : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;
    [SerializeField] EventSystem eventSystem;
    [SerializeField] DialogueSystem dialogueSystem; // Agrega una referencia al DialogueSystem

    public bool isPaused = false;
    public bool executeResume = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && dialogueSystem.isActivePanelDialogueSystem == false)
        {
            Pause();
            isPaused = true;
            DisableEventSystem();
            PauseDialogue(); // Pausa el sistema de di�logo al pausar el juego.
        }
    }

    private void LateUpdate()
    {
        if (executeResume)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
            isPaused = false;
            executeResume = false;
            EnableEventSystem();
            ResumeDialogue(); // Reanuda el sistema de di�logo al reanudar el juego.
        }
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        executeResume = true;
    }

    public void Mainmenu(string scenename)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(scenename);
    }

    private void DisableEventSystem()
    {
        if (eventSystem != null)
        {
            eventSystem.enabled = false;
        }
    }

    private void EnableEventSystem()
    {
        if (eventSystem != null)
        {
            eventSystem.enabled = true;
        }
    }

    private void PauseDialogue()
    {
        if (dialogueSystem != null)
        {
            dialogueSystem.enabled = false; // Desactiva el componente del sistema de di�logo.
        }
    }

    private void ResumeDialogue()
    {
        if (dialogueSystem != null)
        {
            dialogueSystem.enabled = true; // Activa el componente del sistema de di�logo.
        }
    }
}