using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ExitWindow : MonoBehaviour
{
    public GameObject selected;
    public GameObject old;
    public EventSystem eventSystem;

    private void OnEnable()
    {
        eventSystem.SetSelectedGameObject(selected);
    }

    private void OnDisable()
    {
        eventSystem.SetSelectedGameObject(old);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
