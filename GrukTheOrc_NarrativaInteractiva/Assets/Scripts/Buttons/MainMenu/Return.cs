using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Return : MonoBehaviour
{

    // Update is called once per frame
    public void Back(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
