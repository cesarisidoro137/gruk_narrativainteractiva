using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_control : MonoBehaviour
{
    private GameObject lastselected;
    private EventSystem eventsystem;

    void Start()
    {
        // Cursor.visible = false;
        eventsystem = GetComponent<EventSystem>();
        lastselected = eventsystem.firstSelectedGameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (eventsystem.currentSelectedGameObject == null)
        {
            eventsystem.SetSelectedGameObject(lastselected);
        }
        else
        {
            lastselected = eventsystem.currentSelectedGameObject;
        }
    }
}
