using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int runas;
    private Rigidbody2D rb2d;
    [SerializeField]
    public float movespeed;
    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private Transform attackPoint;
    [SerializeField]
    private float attackRange;
    [SerializeField]
    private LayerMask attackLayer;
    [SerializeField]
    private int dmgQuantity;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private float speedmultiplier;
    private bool movingright;
    public int quantityofCoins = 0;
    public int healthPoints;
    public int maxHP;
    public GameObject[] HUDhearts;
    public GameObject HUDbloqueo;
    public GameObject HUDdash;
    private bool dead = false;
    private bool canuseblock = true;
    private bool canusedash = true;
    public bool canbedmg = true;
    public float dashForce;
    private bool canAttack = true; 
    public int ability = 0;
    private Animator animator;
    private bool isDashing;
    private float dashTimer;
    [SerializeField]
    private float maxDashTimer;
    [SerializeField]
    private Animator animatorEscudo;
    public static AudioSource au;

    private bool isGrounded;
   // [SerializeField]
    //private float jumpForce;
    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private LayerMask groundLayerMask;
    [SerializeField]
    private float groundCheckRadius;

    // Start is called before the first frame update
    void Start()
    {
        au = GetComponent<AudioSource>();
        if (ability == 1)
        {
            HUDdash.SetActive(true);
            HUDbloqueo.SetActive(false);
        }
        else if (ability == 2)
        {
            HUDbloqueo.SetActive(true);
            HUDdash.SetActive(false);
        }
        else
        {
            HUDdash.SetActive(false);
            HUDbloqueo.SetActive(false);
        }
        movingright = true;
        if (PlayerPrefs.GetFloat("checkPointPositionX") != 0)
        {
            transform.position = (new Vector2(PlayerPrefs.GetFloat("checkPointPositionX"), PlayerPrefs.GetFloat("checkPointPositionY")));
            ability = PlayerPrefs.GetInt("poderelegido");
            quantityofCoins = PlayerPrefs.GetInt("money");
            maxHP= PlayerPrefs.GetInt("maxhealth");
            healthPoints = PlayerPrefs.GetInt("maxhealth");
            runas = PlayerPrefs.GetInt("Runas");
        }
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (menuPausa.GetComponent<Pause_Menu>().isPaused == false)
        if (Time.timeScale > 0)
        {
            Move();
            Jump();
            AttackEnemies();
            Blocking();
            Dash();
            if (dead == true)
            {
                //animator.SetTrigger("isDeath");
                //SceneManager.LoadScene("MenuDerrota");
            }
            GroundCheck();
           /* if (isGrounded == false)
            {
                /animator.SetBool("IsJumping", true);
            }*/
        }
    }



    private void OnDrawGizmosSelected()
    {
        if (groundCheck)
        {
            Gizmos.color = Color.gray;
            Gizmos.DrawSphere(groundCheck.position, groundCheckRadius);
        }

    }
    void GroundCheck()
    {
        Collider2D collider = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayerMask);

        if (collider && !isGrounded)
        {
            //animator.SetBool("IsJumping", true);
        }

        if (collider)
        {
            isGrounded = true;
            if (rb2d.velocity.y <= 0)
            {
                animator.SetBool("IsJumping", false);
            }

        }
        else
        {
            isGrounded = false;
        }

    }
    void InAnimationDeath()
    {
        movespeed = 0f;
    }
    void PostAnimationDeath()
    {
        SceneManager.LoadScene("MenuDerrota");
    }
    void Move()
    {
        if (Input.GetKey("right")||Input.GetKey(KeyCode.L))
        {
            rb2d.velocity = new Vector2(movespeed, rb2d.velocity.y);
            transform.localScale = new Vector3(1, 1, 1);
            movingright = true;
            if (isGrounded == true)
            {
               // animator.SetBool("IsJumping", false);
                animator.SetBool("OnMovement", true);
            }

        }
        else if (Input.GetKey("left") || Input.GetKey(KeyCode.J))
        {
            rb2d.velocity = new Vector2(-movespeed, rb2d.velocity.y);
            transform.localScale = new Vector3(-1, 1, 1);
            movingright = false;
            if (isGrounded == true)
            {
             //  animator.SetBool("IsJumping", false);
                animator.SetBool("OnMovement", true);
            }
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            if (isGrounded == true)
            {
              //  animator.SetBool("IsJumping", false);
                animator.SetBool("OnMovement", false);
            }
        }
    }

    void Jump()
    {
        if (isGrounded && Input.GetKeyDown(KeyCode.UpArrow) || isGrounded && Input.GetKeyDown(KeyCode.I))
        {
            animator.SetBool("IsJumping", true);
            SFXController.GetSFX().PlaySound("Salto", au);
            rb2d.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            //animator.SetBool("IsJumping", false);
        }
    }

    public void Recievedmg(int a)
    {
        if (canbedmg == true)
        {

            canbedmg = false;
            StartCoroutine(DmgBlinking());
            healthPoints = healthPoints - a;
            HUDhearts[healthPoints].gameObject.SetActive(false);
            if (healthPoints < 1)
            {
                dead = true;
                SceneManager.LoadScene("MenuDerrota");
                //animator.SetTrigger("isDeath");
            }
        }

    }

    public void GainLife(int a)
    {
        if (healthPoints < maxHP)
        {
            SFXController.GetSFX().PlaySound("Vida", au);
            healthPoints = healthPoints + a;
            HUDhearts[healthPoints-1].gameObject.SetActive(true);
        }
    }
    public void GainMaxLife(int a)
    {
        maxHP = maxHP + 1;
        if(healthPoints < 3)
        {
            healthPoints = 3;
            HUDhearts[0].gameObject.SetActive(true);
            HUDhearts[1].gameObject.SetActive(true);
            HUDhearts[2].gameObject.SetActive(true);
        }
        healthPoints = healthPoints + 1;
        quantityofCoins = quantityofCoins - a;
        HUDhearts[healthPoints-1].gameObject.SetActive(true);
    }

    public void AttackEnemies()
    {
        Collider2D collider = Physics2D.OverlapCircle(attackPoint.position, attackRange, attackLayer);
        if (Input.GetKeyDown("z") && canAttack == true)
        {
            // Tiempo de inmunidad del jugador.

            StartCoroutine(ChangePlayerLayerTemporarily(0.3f));
            
            SFXController.GetSFX().PlaySound("Ataque", au);
            StartCoroutine(CooldownAttack());
            animator.SetTrigger("IsAttacking");
            if (collider)
            {
                SFXController.GetSFX().PlaySound("AtaqueConDa�o", au);
                // Intenta obtener el componente EnemyHealthPoints
                EnemyHealthPoints enemyHealth = collider.GetComponent<EnemyHealthPoints>();

                if (enemyHealth != null)
                {
                    // Si es un enemigo com�n, aplica da�o usando EnemyHealthPoints
                    enemyHealth.ReceiveDamage(dmgQuantity);
                }
                else 
                {
                    // Si no es un enemigo com�n, intenta obtener el componente BossHealth
                    BossHealth bossHealth = collider.GetComponent<BossHealth>();

                    if (bossHealth != null)
                    {
                        // Si es un jefe, aplica da�o usando BossHealth
                        bossHealth.ReceiveDamage(dmgQuantity);
                    }
                    else
                    {
                        // Si no es un enemigo com�n, intenta obtener el componente BossHealth
                        BossHealth_Grimmlock bossHealth_Grimmlock = collider.GetComponent<BossHealth_Grimmlock>();

                        if (bossHealth_Grimmlock != null)
                        {
                            // Si es un jefe, aplica da�o usando BossHealth
                            bossHealth_Grimmlock.ReceiveDamage(dmgQuantity);
                        }
                    }
                }
            }
        }

    }

    // Corrutina para cambiar temporalmente la capa del jugador para hacerse inmortal
    private IEnumerator ChangePlayerLayerTemporarily(float duration)
    {
        gameObject.layer = LayerMask.NameToLayer("Jugador2");
        yield return new WaitForSeconds(duration);
        ResetPlayerLayer();
    }

    // Restablece la capa del jugador a la capa original
    void ResetPlayerLayer()
    {
        gameObject.layer = LayerMask.NameToLayer("Jugador");

    }
    public void ReachedCheckPoint(float x, float y)
    {
        PlayerPrefs.SetFloat("checkPointPositionX", x);
        PlayerPrefs.SetFloat("checkPointPositionY", y);
        PlayerPrefs.SetInt("poderelegido", ability);
        PlayerPrefs.SetInt("money", quantityofCoins);
        PlayerPrefs.SetInt("maxhealth", maxHP);
        PlayerPrefs.SetInt("Runas", runas);
    }

    void Blocking()
    {
        if (ability == 2 && canuseblock == true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                SFXController.GetSFX().PlaySound("Bloqueo", au);
                canuseblock = false;
                GetComponent<SpriteRenderer>().color = Color.blue;
                HUDbloqueo.SetActive(false);
                canbedmg = false;
                StartCoroutine(CooldownBlock());
                animatorEscudo.SetBool("EndBlock", false);
            }
        }

    }

    void Dash()
    {
        if (ability == 1 && canusedash == true)
        {
            if (Input.GetKeyDown(KeyCode.X) && !isDashing)
            {
                SFXController.GetSFX().PlaySound("Dash", au);

                animator.SetTrigger("isDash");
                isDashing = true;
                dashTimer = 0;
                StartCoroutine(CooldownDash());
            }

            if (isDashing)
            {
                Shadows.me.ShadowsKill();
                animator.SetTrigger("isDash");
                if (movingright)
                {
                    rb2d.velocity = new Vector2(
                    movespeed * speedmultiplier,
                    rb2d.velocity.y
                    );
                    dashTimer += Time.deltaTime;
                    if (dashTimer >= maxDashTimer)
                    {
                        isDashing = false;
                        rb2d.gravityScale = 5;
                        canusedash = false;
                    }
                }
                else
                {
                    rb2d.velocity = new Vector2(
                    -movespeed * speedmultiplier,
                    rb2d.velocity.y
                    );
                    dashTimer += Time.deltaTime;
                    if (dashTimer >= maxDashTimer)
                    {
                        rb2d.gravityScale = 5;
                        isDashing = false;
                        canusedash = false;
                    }
                }
                
                
            }

        }
    }

    //muerte por caida
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Caida"))
        {
            dead = true;
            for (int i = 0; i < maxHP; i++)
            {
               // HUDhearts[i].gameObject.SetActive(false);
                SceneManager.LoadScene("MenuDerrota");
                // Oficialmente debe enviar a la pantlla de game over

            }
        }
    }


    // da�o por parpadeo
    IEnumerator DmgBlinking() 
    {
        for (int j = 0; j < 5; j++)
        {
            for (int i = 255; i >= 50; i -= 10)
            {
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, i / 255f);
                yield return new WaitForSeconds(0.01f);
            }
            for (int i = 50; i <= 255; i += 10)
            {
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, i / 255f);
                yield return new WaitForSeconds(0.01f);
            }
        }
       
        canbedmg = true;
        yield return null;
    }

    IEnumerator CooldownBlock()
    {
        animator.SetTrigger("isBlock");
        animatorEscudo.SetBool("InitBlock",true);
        for (int j = 0; j <= 150; j += 1)
        {
            //animator.SetTrigger("isBlock");
            yield return new WaitForSeconds(0.05f);
        }
        canbedmg = true;
        GetComponent<SpriteRenderer>().color = Color.white;
        animatorEscudo.SetBool("InitBlock", false);
        animatorEscudo.SetBool("EndBlock",true);
        for (int i = 0; i <= 275; i += 1)
        {
        yield return new WaitForSeconds(0.01f);
        }
      HUDbloqueo.SetActive(true);
      canuseblock = true;
      yield return null;
    }
    IEnumerator CooldownDash()
    {
        HUDdash.SetActive(false);
        for (int i = 0; i <= 400; i++)
        {
          yield return new WaitForSeconds(0.01f);
        }
        HUDdash.SetActive(true);
        canusedash = true;
        yield return null;
    }

    IEnumerator CooldownAttack()
    {
        canAttack = false;
        for (int i = 0; i <= 30; i += 1)
        {
            yield return new WaitForSeconds(0.01f);
        }
        canAttack = true;
        yield return null;

    }

}