using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDBosses : MonoBehaviour
{
    public GameObject bossHUD; // Asigna el HUD del boss en el inspector
    public GameObject bossObject; // Asigna el objeto del boss en el inspector

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            ActivateBossHUD();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            DeactivateBossHUD();
        }
    }

    void ActivateBossHUD()
    {
        if (bossHUD != null)
        {
            bossHUD.SetActive(true);

            // Activar el objeto del boss (si no est� activado) para que comience su l�gica.
            if (bossObject != null && !bossObject.activeSelf)
            {
                bossObject.SetActive(true);
            }
        }
    }

    void DeactivateBossHUD()
    {
        if (bossHUD != null)
        {
            bossHUD.SetActive(false);

            // Activar el objeto del boss cuando el jugador sale del trigger.
            if (bossObject != null && bossObject.activeSelf)
            {
                bossObject.SetActive(false);
            }
        }
    }
}