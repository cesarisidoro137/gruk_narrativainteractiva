using UnityEngine;

public class AutoStartDialogue : MonoBehaviour
{
    public Dialogue dialogueToStart;

    void Start()
    {
        if (dialogueToStart != null)
        {
            // Iniciar el di�logo
            FindObjectOfType<DialogueSystem>().StartDialogue(dialogueToStart);
        }
    }
}
