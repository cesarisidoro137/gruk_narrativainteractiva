using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // Pausar el movimiento del jugador (ajusta seg�n tus necesidades)
            Player player = collision.GetComponent<Player>();
            if (player != null)
            {
                // player.enabled = false;
               // Time.timeScale = 0f;
                //  player.movespeed = 0;
                // player.
            }

            // Iniciar el di�logo
            FindObjectOfType<DialogueSystem>().StartDialogue(dialogue);

            // Desactivar el collider para evitar que se inicie m�ltiples veces
            GetComponent<Collider2D>().enabled = false;

            // Destruir el objeto que tiene este script
            Destroy(gameObject);
        }
    }
}
