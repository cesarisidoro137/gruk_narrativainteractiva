using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueSystem : MonoBehaviour
{
    public GameObject dialoguePanel;
    public TextMeshProUGUI regularDialogueText;
    public TextMeshProUGUI characterDialogueText;
    public Image characterSprite;
    public Button[] optionButtons;
    public float typingSpeed = 0.05f;

    private Dialogue currentDialogue;
    private int currentPageIndex;
    private bool isTyping;
    private bool optionsReady;

    public bool isActivePanelDialogueSystem;

    // Nuevo campo para almacenar la referencia al objeto del escenario
    public GameObject objectToExecuteMethods;

    private bool isTimePaused = false;



    void Start()
    {
        dialoguePanel.SetActive(false);
        if (SceneManager.GetActiveScene().name == "Escenario_Dise�oLvl")
        {
            Time.timeScale = 1f;
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        currentDialogue = dialogue;
        currentPageIndex = 0;
        optionsReady = false;

        dialoguePanel.SetActive(true);
        isActivePanelDialogueSystem = true;

        // Aseg�rate de que ambos textos est�n en blanco al inicio del di�logo
        regularDialogueText.text = "";
        characterDialogueText.text = "";

        // Inicia el tiempo de tipeo inmediatamente al comenzar el di�logo
        StartCoroutine(TypeText(currentDialogue.pages[currentPageIndex].characterSprite != null ? characterDialogueText : regularDialogueText, currentDialogue.pages[currentPageIndex].text));

        // Pausar el juego solo si no est� ya pausado
        if (!isTimePaused)
        {
            Time.timeScale = 0f;
            isTimePaused = true;
        }

        if (currentDialogue.pages[currentPageIndex].characterSprite != null)
        {
            characterSprite.sprite = currentDialogue.pages[currentPageIndex].characterSprite;
            characterSprite.gameObject.SetActive(true);
        }
        else
        {
            characterSprite.gameObject.SetActive(false);
        }
        // Llamar a SwitchTextObjects tambi�n al iniciar el di�logo
        SwitchTextObjects(currentDialogue.pages[currentPageIndex].characterSprite != null);
    }

    IEnumerator TypeText(TextMeshProUGUI textMeshPro, string text)
    {
        isTyping = true;
        textMeshPro.text = "";

        foreach (char letter in text.ToCharArray())
        {
            textMeshPro.text += letter;
            yield return new WaitForSecondsRealtime(typingSpeed);
        }

        isTyping = false;
        optionsReady = true;

        SwitchTextObjects(currentDialogue.pages[currentPageIndex].characterSprite != null);

        SetupOptions();
    }

    void SetupOptions()
    {
        HideOptions();

        if (currentPageIndex < currentDialogue.pages.Length)
        {
            DialoguePage currentPage = currentDialogue.pages[currentPageIndex];

            if (currentPage.hasOption && currentPage.options != null && currentPage.options.Length > 0 && optionsReady)
            {
                for (int i = 0; i < Mathf.Min(optionButtons.Length, currentPage.options.Length); i++)
                {
                    optionButtons[i].gameObject.SetActive(true);
                    optionButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = currentPage.options[i].buttonText;

                    int index = i;
                    optionButtons[i].onClick.AddListener(() => OnOptionSelected(index));
                }
            }
        }
    }

    void HideOptions()
    {
        foreach (Button button in optionButtons)
        {
            button.gameObject.SetActive(false);
            button.onClick.RemoveAllListeners();
        }
    }

    void OnOptionSelected(int optionIndex)
    {
        if (currentPageIndex < currentDialogue.pages.Length)
        {
            DialoguePage currentPage = currentDialogue.pages[currentPageIndex];

            if (currentPage.hasOption && currentPage.options != null && currentPage.options.Length > optionIndex)
            {
                DialoguePage.DialogueOption selectedOption = currentPage.options[optionIndex];

                if (selectedOption.nextDialogue != null)
                {
                    // Iniciar el di�logo si hay un pr�ximo di�logo
                    StartDialogue(selectedOption.nextDialogue);
                }
                else if (selectedOption.shouldCloseDialogue)
                {
                    // Cerrar el di�logo si se especifica
                    EndDialogue();
                }
                else if (!string.IsNullOrEmpty(selectedOption.methodName))
                {
                    // Llamar al m�todo desde el script MethodsForDialogueSystem
                    MethodsForDialogueSystem script = objectToExecuteMethods.GetComponent<MethodsForDialogueSystem>();

                    // Verificar si se encontr� el script
                    if (script != null)
                    {
                        // Llamar al m�todo espec�fico en el script por su nombre
                        script.SendMessage(selectedOption.methodName);

                        // Cerrar el di�logo si se especifica
                         EndDialogue();

                    }
                    else
                    {
                        Debug.LogWarning("El objeto del escenario no tiene el script MethodsForDialogueSystem necesario.");
                    }
                }
                else if (selectedOption.sceneButton != null)
                {
                    // Ejecutar el bot�n en la escena si est� asignado
                    selectedOption.sceneButton.onClick.Invoke();

                    // Cerrar el di�logo si se especifica
                    if (selectedOption.shouldCloseDialogue)
                    {
                        EndDialogue();
                    }
                }
            }
            else
            {
                currentPageIndex++;
                optionsReady = false;
                StartCoroutine(TypeText(currentDialogue.pages[currentPageIndex].characterSprite != null ? characterDialogueText : regularDialogueText, currentDialogue.pages[currentPageIndex].text));

                if (currentDialogue.pages[currentPageIndex].characterSprite != null)
                {
                    characterSprite.sprite = currentDialogue.pages[currentPageIndex].characterSprite;
                    characterSprite.gameObject.SetActive(true);
                    SwitchTextObjects(true);
                }
                else
                {
                    characterSprite.gameObject.SetActive(false);
                    SwitchTextObjects(false);
                }
            }
        }

        HideOptions();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isTyping)
            {
                StopAllCoroutines();
                if (currentDialogue.pages[currentPageIndex].characterSprite != null)
                {
                    characterDialogueText.text = currentDialogue.pages[currentPageIndex].text;
                }
                else
                {
                    regularDialogueText.text = currentDialogue.pages[currentPageIndex].text;
                }
                isTyping = false;
                optionsReady = true;
                SwitchTextObjects(currentDialogue.pages[currentPageIndex].characterSprite != null);
                SetupOptions();
            }
            else if (currentPageIndex < currentDialogue.pages.Length - 1)
            {
                if (currentDialogue.pages[currentPageIndex].hasOption)
                {
                    return;
                }

                currentPageIndex++;
                optionsReady = false;
                StartCoroutine(TypeText(currentDialogue.pages[currentPageIndex].characterSprite != null ? characterDialogueText : regularDialogueText, currentDialogue.pages[currentPageIndex].text));

                if (currentDialogue.pages[currentPageIndex].characterSprite != null)
                {
                    characterSprite.sprite = currentDialogue.pages[currentPageIndex].characterSprite;
                    characterSprite.gameObject.SetActive(true);
                    SwitchTextObjects(true);
                }
                else
                {
                    characterSprite.gameObject.SetActive(false);
                    SwitchTextObjects(false);
                }
            }
            else
            {
                if (!currentDialogue.pages[currentPageIndex].hasOption)
                {
                    EndDialogue();
                }
            }
        }
    }

    void EndDialogue()
    {
        // Obtener la referencia al objeto del escenario asociado al cerrar el di�logo
        GameObject objectToExecuteMethods = this.objectToExecuteMethods;

        // Obtener el nombre del m�todo desde la p�gina actual
        string methodNameToExecute = currentDialogue.pages[currentPageIndex].methodToExecuteOnClose;

        // Verificar si se ha asignado un objeto del escenario
        if (objectToExecuteMethods != null && !string.IsNullOrEmpty(methodNameToExecute))
        {
            // Obtener el script del objeto
            MethodsForDialogueSystem script = objectToExecuteMethods.GetComponent<MethodsForDialogueSystem>();

            // Verificar si se encontr� el script
            if (script != null)
            {
                // Llamar al m�todo espec�fico en el script por su nombre
                script.SendMessage(methodNameToExecute);
            }
            else
            {
                Debug.LogWarning("El objeto del escenario no tiene el script necesario.");
            }
        }

        isActivePanelDialogueSystem = false;
        dialoguePanel.SetActive(false);

        // Reanudar el juego solo si estaba pausado por este di�logo
        if (isTimePaused)
        {
            Time.timeScale = 1f;
            isTimePaused = false;
        }
    }

    void SwitchTextObjects(bool hasCharacterSprite)
    {
        regularDialogueText.gameObject.SetActive(!hasCharacterSprite);
        characterDialogueText.gameObject.SetActive(hasCharacterSprite);
    }
}