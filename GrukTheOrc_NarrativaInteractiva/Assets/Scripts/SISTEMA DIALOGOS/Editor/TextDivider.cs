using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

[CustomEditor(typeof(Dialogue))]
public class TextDivider : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Dividir Texto en P�ginas"))
        {
            Dialogue dialogue = (Dialogue)target;
            DivideTextIntoPages(dialogue);
            EditorUtility.SetDirty(dialogue);
        }
    }

    private void DivideTextIntoPages(Dialogue dialogue)
    {
        List<DialoguePage> pages = new List<DialoguePage>();

        string[] paragraphs = dialogue.text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

        foreach (string paragraph in paragraphs)
        {
            int charactersPerPage = dialogue.charactersPerPage;

            // Dividir el p�rrafo en segmentos de acuerdo al n�mero de caracteres por p�gina
            List<string> segments = new List<string>(Regex.Split(paragraph, @"(?<=\W)"));

            DialoguePage page = new DialoguePage();
            page.text = "";

            foreach (string segment in segments)
            {
                if (page.text.Length + segment.Length <= charactersPerPage)
                {
                    page.text += segment;
                }
                else
                {
                    // Agregar la p�gina actual y comenzar una nueva
                    pages.Add(page);
                    page = new DialoguePage { text = segment };
                }
            }

            // Agregar la �ltima p�gina
            pages.Add(page);
        }

        // Asignar las p�ginas al di�logo
        dialogue.pages = pages.ToArray();
    }
}