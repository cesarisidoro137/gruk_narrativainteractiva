using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MethodsForDialogueSystem : MonoBehaviour
{
    // Puedes agregar cualquier variable que necesites para las l�gicas de tus m�todos
    public int exampleVariable;
    public Player player;

    public GameObject puertaSalida;
    public GameObject rutaOrgullo;
    public GameObject positionKorg;

    public GameObject dialogoPorReflexionar;

    public GameObject blockItem;
    public GameObject Itemdash;

    public GameObject puertaRutaBloqueo;
    public GameObject puertaRutaDash;

    //public BossUiGrimmlock bossUiGrimmlock;

    public GrimmlockBoss3 grimmlockScript;
    public void RutaOrgullo()
    {
        puertaSalida.SetActive(false);
        rutaOrgullo.SetActive(true);
    }

    public void VolverConKorg()
    {
        player.transform.position = positionKorg.transform.position;
        dialogoPorReflexionar.SetActive(true);
        rutaOrgullo.SetActive(false);
    }

    public void ObtenerProteger()
    {
        blockItem.SetActive(true);
        player.ability = 2;
        puertaSalida.SetActive(false);
        puertaRutaBloqueo.SetActive(false);
    }

    public void ObtenerDash()
    {
        Itemdash.SetActive(true);
        player.ability = 1;
        puertaSalida.SetActive(false);
        puertaRutaDash.SetActive(false);
    }


    public void LucharOrgullo()
    {
        SceneManager.LoadScene("RutaOrgullo_C");
    }

    public void ArremeterContraAzog()
    {
        SceneManager.LoadScene("RutaCastillo_C");
    }

    public void ConcordarConKorg()
    {
        SceneManager.LoadScene("RutaVerdaderoRutaCueva_C");
    }

    public void PelearContraGrimmlock()
    {
        grimmlockScript.enabled = true; // Activa el script del boss
    }

    public void ConcederRunas()
    {
        player.runas = 1;
    }
    public void ExampleMethod()
    {
        player.movespeed = 10f;
       // Debug.Log("Ejemplo de m�todo ejecutado desde el di�logo.");
        // Agrega cualquier l�gica que desees realizar en este m�todo
    }

    // Otro ejemplo de m�todo con par�metros
    public void CustomMethodWithParameters(string message, int value)
    {
        Debug.Log($"Mensaje: {message}, Valor: {value}");
        // Agrega cualquier l�gica que desees realizar en este m�todo con par�metros
    }

    // Puedes seguir agregando m�s m�todos seg�n tus necesidades
}
