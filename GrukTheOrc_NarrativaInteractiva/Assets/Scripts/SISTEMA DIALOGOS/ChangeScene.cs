using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public string nameScene;
   // public AudioSource SoundSelection;

    public void Exit()
    {
       // SoundSelection.Play();
        PlayerPrefs.DeleteAll();
        Application.Quit();
    }
    public void Change()
    {
      //  SoundSelection.Play();
       // Time.timeScale = 1;
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(nameScene);
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayerPrefs.DeleteAll();
            Change();
        }

    }
}
