using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleAzog : MonoBehaviour
{
    public BossHealth azog;
    //public Dialogue dialogue;
    public int healthForDialogue;
    public GameObject triggerDialogue;

    // Update is called once per frame
    void Update()
    {
        if(triggerDialogue != null)
        {
            if (azog.healthPoints == healthForDialogue)
            {
                triggerDialogue.SetActive(true);
            }
        }

    }
}
