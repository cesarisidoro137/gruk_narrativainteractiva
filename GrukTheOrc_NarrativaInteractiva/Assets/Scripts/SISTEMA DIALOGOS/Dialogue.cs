using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue")]
[System.Serializable]
public class DialogueOption
{
    public string optionText;
    public Dialogue nextDialogue;
}


[System.Serializable]
public class Dialogue : ScriptableObject
{
    public string dialogueName; // Nuevo campo para el nombre del di�logo (opcional)

    [TextArea(3, 10)]
    public string text;

    [Range(1, 1000)]
    public int charactersPerPage; // Nuevo campo para charactersPerPage

    public DialoguePage[] pages;
}