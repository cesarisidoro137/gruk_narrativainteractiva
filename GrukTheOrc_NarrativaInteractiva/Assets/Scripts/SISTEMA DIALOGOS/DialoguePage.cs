using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DialoguePage
{
    [TextArea(3, 10)]
    public string text;
    public Sprite characterSprite;
    public bool hasOption;
    public DialogueOption[] options;

    private int maxCharactersPerPage;

    // Nuevo campo para el nombre del m�todo que se ejecutar� al cerrar el di�logo
    public string methodToExecuteOnClose;

    public void SplitText()
    {
        if (text.Length <= maxCharactersPerPage)
        {
            return;
        }

        text = text.Trim();

        int endIndex = maxCharactersPerPage;

        while (endIndex < text.Length && text[endIndex] != ' ')
        {
            endIndex++;
        }

        string remainingText = text.Substring(endIndex).Trim();
        text = text.Substring(0, endIndex).Trim();

        if (!string.IsNullOrEmpty(remainingText))
        {
            DialoguePage nextPage = new DialoguePage
            {
                text = remainingText,
                characterSprite = characterSprite,
                hasOption = hasOption,
                options = options,
                maxCharactersPerPage = maxCharactersPerPage
            };

            //options = new DialogueOption[] { new DialogueOption { buttonText = "Continue", nextDialogue = nextPage } };
        }
    }

    [System.Serializable]
    public class DialogueOption
    {
        public string buttonText;
        public Dialogue nextDialogue;
        public Button sceneButton; // Nuevo campo para el bot�n en la escena

        // Nuevo nombre del m�todo a ejecutar
        public string methodName;

        // Nuevo booleano para indicar si deber�a cerrar el di�logo
        public bool shouldCloseDialogue;
    }
}