using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SFXController.GetSFX().PlaySound("Moneda", Player.au);
            collision.gameObject.GetComponent<Player>().quantityofCoins = collision.gameObject.GetComponent<Player>().quantityofCoins + 1;
            Destroy(this.gameObject);
        }
    }
}
