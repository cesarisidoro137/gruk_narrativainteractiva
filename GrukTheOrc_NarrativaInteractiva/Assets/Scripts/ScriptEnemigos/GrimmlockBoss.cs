using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrimmlockBoss : MonoBehaviour
{
    public Transform leftLimit;
    public Transform rightLimit;
    public float walkSpeed = 2f;
    public float dashSpeed = 5f;
    public float dashDuration = 1f;
    public float fatigueDuration = 2f;

    public GameObject player;
    public float detectionRange;

    private enum BossState
    {
        Idle,
        Walk,
        Dash,
        Fatigue
    }

    private BossState currentState = BossState.Idle;
    private float dashTimer = 0f;
    private float fatigueTimer = 0f;

    private void Update()
    {
        switch (currentState)
        {
            case BossState.Idle:
                // Implement logic to detect player within a certain range
                if (PlayerInRange())
                {
                    ChangeState(BossState.Walk);
                }
                break;

            case BossState.Walk:
                transform.Translate(Vector2.right * walkSpeed * Time.deltaTime);

                // Implement logic to transition to Dash state after a certain time
                if (Time.time > dashTimer)
                {
                    ChangeState(BossState.Dash);
                }
                break;

            case BossState.Dash:
                Dash();

                // Implement logic to transition to Fatigue state after dashing
                if (dashTimer <= 0f)
                {
                    ChangeState(BossState.Fatigue);
                }
                break;

            case BossState.Fatigue:
                // Implement logic for the boss being vulnerable
                // Transition to Dash state after a certain time
                if (Time.time > fatigueTimer)
                {
                    ChangeState(BossState.Dash);
                }
                break;
        }
    }

    private void Dash()
    {
        // Ensure the boss is facing the player
        Vector3 playerDirection = (player.transform.position - transform.position).normalized;
        transform.right = playerDirection;

        // Move towards the player with dashSpeed
        transform.Translate(Vector2.right * dashSpeed * Time.deltaTime);

        // Check if the boss has reached the left or right limit
        if (transform.position.x < leftLimit.position.x || transform.position.x > rightLimit.position.x)
        {
            // Reverse direction if the limit is reached
            dashTimer = 0f;
        }

        // Update dashTimer
        dashTimer -= Time.deltaTime;
    }

    private bool PlayerInRange()
    {
        // Check if the player is within a certain distance
        float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
        return distanceToPlayer < detectionRange;
    }

    private void ChangeState(BossState newState)
    {
        currentState = newState;

        switch (newState)
        {
            case BossState.Walk:
                // Set walk-related parameters and timers
                break;

            case BossState.Dash:
                // Set dash-related parameters and timers
                dashTimer = dashDuration;
                break;

            case BossState.Fatigue:
                // Set fatigue-related parameters and timers
                fatigueTimer = Time.time + fatigueDuration;
                break;
        }
    }
}
