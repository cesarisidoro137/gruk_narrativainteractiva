using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol: MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private int healthPoints;
    [SerializeField]
    private float distance;
    private bool movingRight = true;
    [SerializeField]
    private Transform frontCollideDetection;
    [SerializeField]
    private float frontCollideRadius;
    [SerializeField]
    private LayerMask patrolMask;
    [SerializeField]
    private int dmg;
    [SerializeField]
    private LayerMask dmgMask;
    public GameObject moneda;


    void Update()
    {
        Patrol();
        Hacerdmg();
    }
    void Patrol()
    {
        Collider2D collider = Physics2D.OverlapCircle(frontCollideDetection.position, frontCollideRadius, patrolMask);
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(frontCollideDetection.position, Vector2.down, distance);
        if (groundInfo.collider == false || collider == true)
        {
            if (movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }

    void Hacerdmg()
    {
        //Collider2D collider = Physics2D.OverlapCircle(gameObject.transform.position, frontCollideRadius, dmgMask);
        Collider2D collider = Physics2D.OverlapBox(gameObject.transform.position, new Vector2(1.33f, 1.583f), 0, dmgMask);
        if (collider==true)
        {
            if (GameObject.Find("Personaje").GetComponent<Player>().canbedmg == true)
            {
                collider.gameObject.GetComponent<Player>().Recievedmg(dmg);
            }
            
            
        }
    }
    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Jugador>().Recibirdmg(dmg);
        }
    }*/

}

