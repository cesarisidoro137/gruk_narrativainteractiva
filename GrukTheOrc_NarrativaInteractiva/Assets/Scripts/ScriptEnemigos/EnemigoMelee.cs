using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMelee : MonoBehaviour
{
    [SerializeField]
    private GameObject checklimits;
    [SerializeField]
    private Transform returnpoint;
    [SerializeField]
    private float movespeed;
    [SerializeField]
    private int dmg;
    [SerializeField]
    private LayerMask playerMask;
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private float groundDistance;
    [SerializeField]
    private float impactDistance;
    [SerializeField]
    private float checkPlayerDistance;
    [SerializeField]
    private float checkPlayerX;
    private bool movingleft;
    private bool shouldmove = true;
    private bool start = false; 
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool canAttack = true;

    [SerializeField]
    private int direction;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        Collider2D collider = Physics2D.OverlapCircle(transform.position, checkPlayerDistance, playerMask);
        if (collider)
        {
            start = true;
        }
        if (start == true)
        {
            if (Mathf.Abs(rb2d.velocity.x) > 0.1)
            {
                anim.SetBool("isMoving", true);
            }
            else
            {
                anim.SetBool("isMoving", false);
            }
            Hacerdmg();
            Attack();
            if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("MeleeAttack") == false)
            {
                PatrolMelee();
            }
            else
            {
                rb2d.velocity = new Vector2(0, 0);
            }
        }
      
        
    }

    void PatrolMelee()
    {
        Collider2D collider = Physics2D.OverlapCircle(transform.position, checkPlayerDistance, playerMask);
        Collider2D colliderground = Physics2D.OverlapCircle(checklimits.transform.position, impactDistance, groundMask);
        RaycastHit2D checkleft = Physics2D.Raycast(transform.position, Vector2.left, checkPlayerX, playerMask);
        RaycastHit2D checkright = Physics2D.Raycast(transform.position, Vector2.right, checkPlayerX, playerMask);
        RaycastHit2D checkground = Physics2D.Raycast(checklimits.transform.position, Vector2.down, groundDistance, groundMask);

        if (shouldmove)
        {
            if (colliderground || checkground.collider == false)
            {
                shouldmove = false;
            }
            else
            {
                if (collider)
                {
                    if (checkleft)
                    {
                        MoveTowardsTarget(-movespeed, Vector3.left);
                    }
                    else if (checkright)
                    {
                        MoveTowardsTarget(movespeed, Vector3.right);
                    }
                    else
                    {
                        StopMoving();
                    }
                }
                else
                {
                    // Si no hay un obst�culo en la direcci�n actual, contin�a en la misma direcci�n.
                    MoveInCurrentDirection();
                }
            }
        }
        else
        {
            // Si est� en modo "no deber�a moverse", solo verifica si debe cambiar de direcci�n y det�n el movimiento.
            if (ShouldChangeDirection(checkleft, checkright))
            {
                ChangeDirection();
            }

            // Movimiento en la direcci�n actual solo si est� cerca del jugador.
            if (Vector2.Distance(transform.position, returnpoint.position) > 0.5f)
            {
                MoveInCurrentDirection();
            }
            else
            {
                StopMoving();
            }

            // Comprueba si debe volver a activarse el modo de movimiento.
            if (Vector2.Distance(transform.position, returnpoint.position) < 0.5f)
            {
                StopMoving();
                shouldmove = true;
            }
            else if ((movingleft && checkleft) || (!movingleft && checkright))
            {
                StopMoving();
                shouldmove = true;
            }
        }
    }

    void MoveTowardsTarget(float speed, Vector3 direction)
    {
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
        transform.localScale = direction.x > 0 ? new Vector3(1, 1, 1) : new Vector3(-1, 1, 1);
        anim.SetBool("isMoving", true);
    }

    void StopMoving()
    {
        rb2d.velocity = Vector2.zero;
        anim.SetBool("isMoving", false);
    }

    void MoveInCurrentDirection()
    {
        MoveTowardsTarget(movingleft ? movespeed / 2 : -movespeed / 2, movingleft ? Vector3.right : Vector3.left);
    }

    bool ShouldChangeDirection(RaycastHit2D checkleft, RaycastHit2D checkright)
    {
        // Verifica si el enemigo debe cambiar de direcci�n.
        return (movingleft && !checkleft) || (!movingleft && !checkright);
    }

    void ChangeDirection()
    {
        // Cambia la rotaci�n del enemigo en el eje Y.
        transform.eulerAngles = new Vector3(0, movingleft ? 0 : -180, 0);
        movingleft = !movingleft;
    }

    void Hacerdmg()
    {
        Collider2D collider = Physics2D.OverlapBox(gameObject.transform.position, new Vector2(0.8f, 0.8f), 0, playerMask);
        if (collider == true)
        {
            if (collider != null)
            {
                if (GameObject.Find("Personaje").GetComponent<Player>().canbedmg == true)
                {
                    collider.gameObject.GetComponent<Player>().Recievedmg(dmg);
                }
            }
        }
    }

    void Attack()
    {
        Collider2D colliderPJ = Physics2D.OverlapCircle(checklimits.transform.position, impactDistance, playerMask);
        if (colliderPJ && canAttack)
        {
            StartCoroutine(CooldownAttack());
            anim.SetTrigger("isAttacking");
        }
    }

    IEnumerator CooldownAttack()
    {
        canAttack = false;
        for (int i = 0; i <= 100; i+= 1)
        {
            yield return new WaitForSeconds(0.01f);
        }
        canAttack = true;
        yield return null; 
        
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, checkPlayerDistance);
    }
}
