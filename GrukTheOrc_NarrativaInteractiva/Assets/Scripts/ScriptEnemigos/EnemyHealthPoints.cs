using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthPoints : MonoBehaviour
{
    [SerializeField]
    private int healthPoints;
    public GameObject moneda;
    [SerializeField]
    private SpriteRenderer sprite;
    private Animator anim;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }
    public void ReceiveDamage(int a)
    {
        StartCoroutine(DamageTaken());
        healthPoints = healthPoints - a;
        if (healthPoints <= 0)
        {
            anim.SetTrigger("isDeath");

        }
    }

    IEnumerator DamageTaken()
    {
        sprite.color = Color.red;
        for (int i = 0; i <= 60; i++)
        {
            yield return new WaitForSeconds(0.01f);
        }
        sprite.color = Color.white;
        yield return null; 
    }

    void PostAnimationDeath()
    {

        Instantiate(moneda, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
