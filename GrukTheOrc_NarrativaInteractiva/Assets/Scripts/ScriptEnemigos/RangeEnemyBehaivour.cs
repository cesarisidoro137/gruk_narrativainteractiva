using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemyBehaivour : MonoBehaviour
{
    public float range;
    public LayerMask targetLayerMask;
    private Animator anim;

    public Transform target;
    bool Detected = false;

    public GameObject shootcolor;


    Vector2 direction;
    public GameObject spear;

    public float fire;
    float nextimetofire = 0;

    public Transform shootpoint;

    public float force;

    //public Transform playerTransform; // Variable para asignar el Transform del jugador desde el Inspector
    void Start()
    {
        target = GameObject.FindWithTag("Player").transform; // Asigna autom�ticamente el jugador al iniciar

        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 targetpos = target.position;

        direction = targetpos - (Vector2)transform.position;

        RaycastHit2D rayinfo = Physics2D.Raycast(transform.position, direction, range, targetLayerMask);

        if (rayinfo)
        {
            if (rayinfo.collider.gameObject.tag == "Player")
            {
                if (Detected == false)
                {
                    Detected = true;
                   // shootcolor.GetComponent<SpriteRenderer>().color = Color.red;
                }
            }
        }
        else
        {
            if (Detected == true)
            {
                Detected = false;
               // shootcolor.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }

        if (Detected)
        {
            // Almacena el signo de la direcci�n en x
            float directionSign = Mathf.Sign(direction.x);

            // Aplica el signo solo a la direcci�n sin afectar la magnitud
            Vector3 newRotation = new Vector3(0, directionSign == 1 ? 0 : 180, 0);

            // Asigna la nueva rotaci�n al objeto
            transform.localRotation = Quaternion.Euler(newRotation);

            if (Time.time > nextimetofire)
            {
                nextimetofire = Time.time + 1 / fire;
                ThrowSpear();
                //anim.Play("IdleRange");
                //shoot();
            }
        }
    }

    void shoot()
    {
        // Calcula la rotaci�n necesaria para apuntar hacia la posici�n del jugador
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        GameObject bulletins = Instantiate(spear, shootpoint.position, rotation);
        anim.Play("IdleRange");
        bulletins.GetComponent<Rigidbody2D>().AddForce(direction.normalized * force);
        //anim.SetBool("isShooting", false);
        //anim.SetTrigger("isShooting");
    }

    public void ThrowSpear()
    {
        anim.Play("ThrowSpear");

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, range);
    }


}
