using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrimmlockBoss3 : MonoBehaviour
{
    public float range;
    public LayerMask targetLayerMask;
    public Transform target;
    bool Detected = false;
    Vector2 direction;

    public Transform leftLimit;
    public Transform rightLimit;
    public float walkSpeed = 2f;
    public float dashSpeed = 5f;
    public float dashDuration = 0.5f;
    public float fatigueDuration = 3f;

    private Transform player;
    [SerializeField]
    private bool isWalking = false;
    [SerializeField]
    private bool isDashing = false;

    [SerializeField]
    private int dmg;
    [SerializeField]
    private LayerMask dmgMask;

    [SerializeField]
    private SpriteRenderer sprite;

    [SerializeField]
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        StartCoroutine(BossStateMachine());
    }
    void Hacerdmg()
    {
        //Collider2D collider = Physics2D.OverlapCircle(gameObject.transform.position, frontCollideRadius, dmgMask);
        Collider2D collider = Physics2D.OverlapBox(gameObject.transform.position, new Vector2(1.33f, 1.583f), 0, dmgMask);
        if (collider == true)
        {
            if (GameObject.Find("Personaje").GetComponent<Player>().canbedmg == true)
            {
                collider.gameObject.GetComponent<Player>().Recievedmg(dmg);
            }


        }
    }

    IEnumerator BossStateMachine()
    {
        while (true)
        {

            yield return StartCoroutine(WalkState());
            yield return StartCoroutine(DashState());
            yield return StartCoroutine(FatigueState());
        }
    }

    IEnumerator WalkState()
    {

        isWalking = true;
        dashSpeed = 0;
        // yield return new WaitForSeconds(Random.Range(2f, 5f)); // Tiempo que camina
         yield return new WaitForSeconds(1f); // Tiempo que camina
        dashSpeed = 5f;
        isWalking = false;
    }

    IEnumerator DashState()
    {
        isDashing = true;

        for (int i = 0; i < 3; i++)
        {
            Dash(leftLimit.position, -1); // Dash hacia la izquierda
            yield return new WaitForSeconds(dashDuration);

            Dash(rightLimit.position, 1); // Dash hacia la derecha
            yield return new WaitForSeconds(dashDuration);
        }

        isDashing = false;
    }

    void Dash(Vector3 targetPosition, int direction)
    {
        animator.SetTrigger("isAttacking");
        StartCoroutine(MoveTowards(targetPosition, direction));
    }

    IEnumerator MoveTowards(Vector3 targetPosition, int direction)
    {
        float startTime = Time.time;
        Vector3 startPosition = transform.position;

        while (Time.time - startTime < dashDuration)
        {
            float t = (Time.time - startTime) / dashDuration;
            transform.position = Vector3.Lerp(startPosition, targetPosition, t);

            // Invertir el sprite si est� dashando hacia la izquierda
            if (direction < 0)
            {
                sprite.flipX = true;
            }
            else
            {
                sprite.flipX = false;
            }

            yield return null;
        }

        // Aseg�rate de que el objeto termine exactamente en la posici�n objetivo
        transform.position = targetPosition;

        // Restaurar la orientaci�n del sprite al finalizar el dash
        sprite.flipX = false;
    }

    IEnumerator FatigueState()
    {
        animator.SetBool("isFatigued",true);
        // StartCoroutine(ChangePlayerLayerTemporarily(2f));
        // sprite.color = Color.blue;
        // this.gameObject.layer = LayerMask.NameToLayer("Enemigo");
        yield return new WaitForSeconds(fatigueDuration);
        // StartCoroutine(ChangePlayerLayerTemporarily(2f));
        // sprite.color = Color.white;
        animator.SetBool("isFatigued", false);
    }
    void Update()
    {


       // RaycastHit2D rayinfo = Physics2D.Raycast(transform.position, direction, range, targetLayerMask);

      /*  if (rayinfo)
        {
            if (rayinfo.collider.gameObject.tag == "Player")
            {
                Vector2 targetpos = target.position;
                direction = targetpos - (Vector2)transform.position;
                if (Detected == false)
                {
                    Detected = true;

                    StartCoroutine(BossStateMachine());
                }
            }
        }
        else
        {
            if (Detected == true)
            {
                Detected = false;
            }
        }*/

        if (isWalking)
        {
            // Implementa el comportamiento de caminar aqu�
        }

        if (isDashing)
        {
            Hacerdmg();
            // Implementa el comportamiento de dash aqu�
        }
    }
}
