using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private int dmg;

    [SerializeField]
    private float destroyDelay; // Ajusta el tiempo de destrucci�n 
    private void Start()
    {
        // Llama a la funci�n Destroy despu�s de un retraso
        Destroy(this.gameObject, destroyDelay);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verifica si el objeto con el que colision� es el jugador
        if (other.CompareTag("Player"))
        {
            // Causa da�o al jugador
            other.GetComponent<Player>().Recievedmg(dmg);
            Destroy(gameObject);
        }
    }
}
