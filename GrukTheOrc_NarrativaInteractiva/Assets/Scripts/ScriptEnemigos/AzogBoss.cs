using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AzogBoss : MonoBehaviour
{
    public float range;
    public LayerMask targetLayerMask;
    public List<Transform> jumpPoints;  // Lista de puntos a los que el jefe puede saltar

    public Transform target;
    bool Detected = false;

    public GameObject shootcolor;

    Vector2 direction;
    public GameObject spear;

    public float fire;
    float nextimetofire = 0;

    public Transform shootpoint;
    public float force;

    // Nuevas variables para el salto
    public float jumpCooldown = 6f;
    float nextJumpTime = 0f;
    int currentJumpIndex = 0;

    public float jumpHeight = 5f; // Altura m�xima del salto
    public float timeToJumpApex = 0.5f; // Tiempo que tarda en alcanzar la altura m�xima

    float jumpVelocity;
    float gravity;

    public Transform feetPosition;  // Punto en los pies del jefe

    [SerializeField]
    private Animator animator;

    void Start()
    {
       // animator = gameObject.GetComponent<Animator>();
        target = GameObject.FindWithTag("Player").transform;
        CalculateJumpParameters();
    }

    void Update()
    {
        Vector2 targetpos = target.position;
        direction = targetpos - (Vector2)transform.position;

        RaycastHit2D rayinfo = Physics2D.Raycast(transform.position, direction, range, targetLayerMask);

        if (rayinfo)
        {
            if (rayinfo.collider.gameObject.tag == "Player")
            {
                if (Detected == false)
                {
                    Detected = true;
                   // shootcolor.GetComponent<SpriteRenderer>().color = Color.red;
                }

                // Llama al m�todo de salto si ha pasado el tiempo de cooldown
                if (Time.time > nextJumpTime)
                {
                    nextJumpTime = Time.time + jumpCooldown;
                    JumpToNextPoint();
                }
            }
        }
        else
        {
            if (Detected == true)
            {
                Detected = false;
                //shootcolor.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }

        if (Detected)
        {
            float directionSign = Mathf.Sign(direction.x);
            Vector3 newRotation = new Vector3(0, directionSign == 1 ? 0 : 180, 0);
            transform.localRotation = Quaternion.Euler(newRotation);

            if (Time.time > nextimetofire)
            {
                nextimetofire = Time.time + 1 / fire;
                ThrowSpear();
                shoot();
            }
        }
    }

    void JumpToNextPoint()
    {
        if (jumpPoints.Count > 0)
        {
            Vector3 startPoint = transform.position;
            Vector3 endPoint = jumpPoints[currentJumpIndex].position;

            float horizontalDistance = Vector3.Distance(new Vector3(startPoint.x, 0, 0), new Vector3(endPoint.x, 0, 0));
            float verticalDistance = jumpHeight;

            // Calcula la velocidad inicial requerida para alcanzar la altura deseada
            jumpVelocity = 2 * verticalDistance / timeToJumpApex;

            // Calcula la gravedad necesaria para alcanzar la altura deseada en el tiempo especificado
            gravity = -2 * jumpHeight / Mathf.Pow(timeToJumpApex, 2);

            StartCoroutine(MoveToNextPoint(endPoint));

            // Incrementa el �ndice para el pr�ximo salto
            currentJumpIndex = (currentJumpIndex + 1) % jumpPoints.Count;
        }
    }

    IEnumerator MoveToNextPoint(Vector3 endPoint)
    {
        animator.SetTrigger("isJump");
        Vector3 startPoint = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < timeToJumpApex)
        {
            float t = elapsedTime / timeToJumpApex;
            transform.position = Vector3.Lerp(startPoint, endPoint, t);

            // Aplica la f�rmula de movimiento parab�lico para ajustar la altura
            float y = startPoint.y + jumpVelocity * t + 0.5f * gravity * Mathf.Pow(t, 2);
            transform.position = new Vector3(transform.position.x, y, transform.position.z);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        feetPosition.position = endPoint;

    }
    void shoot()
    {
        animator.SetTrigger("isShooting");
    }
    void ThrowSpear()
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        //animator.SetTrigger("isShooting");
        GameObject bulletins = Instantiate(spear, shootpoint.position, rotation);
        bulletins.GetComponent<Rigidbody2D>().AddForce(direction.normalized * force);
    }

    void CalculateJumpParameters()
    {
        // Calcula los par�metros del salto usando la f�rmula de movimiento parab�lico
        jumpVelocity = Mathf.Abs(Physics2D.gravity.y) * timeToJumpApex;
        gravity = -2 * jumpHeight / Mathf.Pow(timeToJumpApex, 2);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, range);
    }
}