using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrimmlockBoss1 : MonoBehaviour
{
    public GameObject player;
    public float walkSpeed;
    public float dashSpeed;
    public float dashCooldown;
    public float fatigueDuration;
    public Transform leftLimit;
    public Transform rightLimit;

    private enum State { Walk, Dash, Fatigue }
    private State currentState;

    private float dashTimer;
    private float fatigueTimer;

    void Start()
    {
        currentState = State.Walk;
        dashTimer = dashCooldown;
        fatigueTimer = 0f;
    }

    void Update()
    {
        switch (currentState)
        {
            case State.Walk:
                Walk();
                break;
            case State.Dash:
                Dash();
                break;
            case State.Fatigue:
                Fatigue();
                break;
        }
    }

    private void Walk()
    {
        if (PlayerInRange())
        {
            dashTimer = dashCooldown;
            currentState = State.Dash;
        }
        else
        {
            transform.Translate(Vector2.right * walkSpeed * Time.deltaTime);
        }
    }

    private void Dash()
    {
        if (dashTimer > 0)
        {
            dashTimer -= Time.deltaTime;
        }
        else
        {
            Vector2 direction = (player.transform.position - transform.position).normalized;
            transform.right = direction;
            transform.Translate(Vector2.right * dashSpeed * Time.deltaTime);

            if (transform.position.x < leftLimit.transform.position.x || transform.position.x > rightLimit.transform.position.x)
            {
                dashTimer = dashCooldown;
                currentState = State.Walk;
            }
        }
    }

    private void Fatigue()
    {
        if (fatigueTimer > 0)
        {
            fatigueTimer -= Time.deltaTime;
        }
        else
        {
            currentState = State.Dash;
        }
    }

    private bool PlayerInRange()
    {
        float distance = Vector2.Distance(transform.position, player.transform.position);
        return distance <= walkSpeed * 10;
    }
}
