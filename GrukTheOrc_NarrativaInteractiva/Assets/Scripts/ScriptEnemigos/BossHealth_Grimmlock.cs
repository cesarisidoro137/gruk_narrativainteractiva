using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class BossHealth_Grimmlock : MonoBehaviour
{
    public float healthPointsMax;
    public Image LifeImg;
    [SerializeField]
    public int healthPoints;
    [SerializeField]
    private SpriteRenderer sprite;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }
    public void ReceiveDamage(int a)
    {
        StartCoroutine(DamageTaken());
        healthPoints = healthPoints - a;
        // vinculacion de la vida del boss para con el ui de vida del boss
        LifeImg.fillAmount = healthPoints / healthPointsMax;
        if (healthPoints == 1)
        {
            SceneManager.LoadScene("FinalRutaCueva_C"); 
            //DestroyBoss();
        }
    }

    IEnumerator DamageTaken()
    {
        sprite.color = Color.red;
        for (int i = 0; i <= 60; i++)
        {
            yield return new WaitForSeconds(0.01f);
        }
        sprite.color = Color.white;
        yield return null;
    }

    private void DestroyBoss()
    {
        if (transform.parent != null)
        {
            Destroy(transform.parent.gameObject); // Destruye el objeto padre si existe
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject); // Si no tiene padre, destruye solo este objeto
        }
    }
}
