using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//al detectar el tag del player se activa la escena de pelea contra el boss
public class BossActivationAzog : MonoBehaviour
{
   public  BossUiAzog bossUiAzog;

    public Dialogue dialogue;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            bossUiAzog.BossActivator();
            Time.timeScale = 0f;

            // Iniciar el di�logo
            FindObjectOfType<DialogueSystem>().StartDialogue(dialogue);

            // Desactivar el collider para evitar que se inicie m�ltiples veces
            GetComponent<Collider2D>().enabled = false;

            // Destruir el objeto que tiene este script
            Destroy(gameObject);

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        CheckGround.isGrounded = false;
    }

}
