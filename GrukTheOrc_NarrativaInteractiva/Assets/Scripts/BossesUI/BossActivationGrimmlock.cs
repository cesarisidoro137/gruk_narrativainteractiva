using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//al detectar el tag del player se activa la escena de pelea contra el boss
public class BossActivationGrimmlock : MonoBehaviour
{
    public BossUiGrimmlock bossUiGrimmlock;
   // public GrimmlockBoss3 grimmlockScript;

    //public Dialogue dialogue;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            bossUiGrimmlock.BossActivator();

           // grimmlockScript.enabled = true; // Activa el script del boss
            //Time.timeScale = 0f;
            // Iniciar el di�logo
            //FindObjectOfType<DialogueSystem>().StartDialogue(dialogue);

            // Desactivar el collider para evitar que se inicie m�ltiples veces
            GetComponent<Collider2D>().enabled = false;
            Destroy(gameObject);

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        CheckGround.isGrounded = false;
    }

}
