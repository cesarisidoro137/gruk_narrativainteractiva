using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalBoss : MonoBehaviour
{

    public float speed = 2.5f;

    public float LifeBoss, currentLife;
    public Image LifeImg;

    public GameObject FirebossPrefab;

    public float shootTimer;

    public float maxShootTimer;
    public float TPtime, CountdownToTP;

    private int direction = 0;
    public Transform[] transforms;

    private Animator animator;
    public AudioSource Deteccionjugador;
    public AudioSource RecibirdaņoF1;
    public AudioSource RecibirdaņoF2;
    public AudioSource Movimiento;
    public AudioSource Teletransportacion;
    public AudioSource AtaqueF1;
    public AudioSource AtaqueF2;
    public AudioSource Metamorfosis;
    public AudioSource SonidoMuerte;



    Transform player;
    Rigidbody2D rb2d;
    //Boss boss;

    //Para ataque cuerpo a cuerpo
    [SerializeField]
    private Transform attackPoint;
    [SerializeField]
    private float attackRadius;
    [SerializeField]
    private LayerMask attackLayerMask;

    [SerializeField]
    private float attackTimer;
    [SerializeField]
    private float maxAttackTimer;

    [SerializeField]
    private float stopMoveTimer;
    [SerializeField]
    private float maxStopMoveTimer;

    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb2d = GetComponent<Rigidbody2D>();
       // boss = GetComponent<Boss>();
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
        {
            return;
        }
        //mientras la vida sea mayor al valor seleccionado se teletransportara y atacara a distancia
        if (currentLife >= 25)
        {

            Fase1();

        }

        // Si su vida esta por debajo del valor seleccionado  perseguira al jugador y dejara de atacar a distancia y transportarse
        else
        {

            Fase2();
        }

    }
    private void Shoot()
    {

        shootTimer += Time.deltaTime;

        AtaqueF1.Play();
        if (shootTimer >= maxShootTimer)
        {

            animator.SetTrigger("isattack");
            GameObject obj = Instantiate(FirebossPrefab);
            obj.transform.position = transform.position;
            shootTimer = 0;
          //  obj.GetComponent<Fireboss>().direction = direction;

        }
    }
    private void Fase1()
    {
        Vector2 target = new Vector2(player.position.x, rb2d.position.y);
        {

            if (player && Vector2.Distance(player.transform.position, transform.position) < 9)

            {


                Deteccionjugador.Play();
                if (player.transform.position.x < transform.position.x)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                    direction = -1;
                    Shoot();
                }

                else
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                    direction = 1;
                    Shoot();
                }
                CountdownToTP -= Time.deltaTime;
                if (CountdownToTP <= 0f)
                {

                    CountdownToTP = TPtime;
                    Teleport();


                }



            }

        }
    }
    private void Fase2()
    {
        if (!Movimiento.isPlaying)
        {
            Movimiento.Play();
            Movimiento.loop = true;
        }

        if (player && Vector2.Distance(player.transform.position, transform.position) < 8)

        {

            if (player.transform.position.x < transform.position.x)
            {
                transform.localScale = new Vector3(1, 1, 1);
                direction = -1;
                Attack();
            }

            else
            {
                transform.localScale = new Vector3(-1, 1, 1);
                direction = 1;
                Attack();
            }
            animator.SetFloat("CurrentSpeed", Mathf.Abs(speed));
            rb2d.velocity = new Vector2(direction * speed, rb2d.velocity.y);
        }


    }



    public void Teleport()
    {
        animator.SetTrigger("tp");
        var initialposition = Random.Range(0, transforms.Length);
        transform.position = transforms[initialposition].position;

    }

    //Ataque cuerpo a cuerpo del boss
    void Attack()
    {
        attackTimer += Time.deltaTime;
        if (attackTimer >= maxAttackTimer)
        {

            attackTimer = 0;
            Collider2D[] collider = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, attackLayerMask);
            if (collider.Length > 0)
            {
                for (int i = 0; i < collider.Length; i++)
                {
                    if (collider[i].gameObject.GetComponent<Player>())
                    {
                     //   collider[i].gameObject.GetComponent<Player>().TakeDamagePlayer(6);
                        animator.SetTrigger("isattack2");
                        AtaqueF2.Play();
                    }
                }
            }
        }

    }

    //Para ver radio de ataque
    private void OnDrawGizmosSelected()
    {
        if (attackPoint)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(attackPoint.position, attackRadius);
        }
    }

    public void TakeDamage(int value)
    {
        // la vida del boss disminuye dependiendo del ataque seleccionado 
        currentLife -= value;
        // vinculacion de la vida del boss para con el ui de vida del boss
        LifeImg.fillAmount = currentLife / LifeBoss;

        if (currentLife < 0f)
        {
            SonidoMuerte.Play();
            animator.Play("Boss_defeat");

            //BossUi.instance.BossDesactivator();


        }
        else
        {
            if (currentLife >= 25)
            {
                RecibirdaņoF1.Play();

                animator.SetTrigger("Damage");

            }
            else
            {
                if (currentLife == 25)
                {
                    animator.SetTrigger("meta");

                }
                else
                {
                    animator.SetTrigger("Damage2");
                }
                RecibirdaņoF2.Play();


            }
        }




    }
    private void DestroyEnemy()
    {


        SceneManager.LoadScene("Win");
    }
    private void Morphosis()
    {

        Metamorfosis.Play();
        animator.SetTrigger("meta");
        stopMoveTimer += Time.deltaTime;
        if (stopMoveTimer >= maxStopMoveTimer)
        {
            stopMoveTimer = 1;
        }

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("AreaRetornoBoss"))
        {
            Teleport();
        }
    }
}
