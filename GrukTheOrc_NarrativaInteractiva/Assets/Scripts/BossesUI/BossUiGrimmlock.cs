using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossUiGrimmlock : MonoBehaviour
{
    public GameObject bossPanel;
    public GameObject walls;

    //public BossUiGrimmlock instance;
    private void Awake()
    {
       /* if (instance == null)
        {
            instance = this;
        }*/
    }
    void Start()
    {
        bossPanel.SetActive(false);
        walls.SetActive(false);
    }

   public void BossActivator()
    {
        bossPanel.SetActive(true);
        walls.SetActive(true);
    }
    public void BossDesactivator()
    {
        bossPanel.SetActive(false);
        walls.SetActive(false);
    }
}
