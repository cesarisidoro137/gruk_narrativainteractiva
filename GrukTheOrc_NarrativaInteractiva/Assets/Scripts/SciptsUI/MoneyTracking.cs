using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MoneyTracking : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private TextMeshProUGUI money;

    // Update is called once per frame
    void Update()
    {
        money.text = player.GetComponent<Player>().quantityofCoins.ToString();
    }
}
