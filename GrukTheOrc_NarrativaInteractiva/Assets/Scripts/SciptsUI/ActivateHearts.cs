using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHearts : MonoBehaviour
{
    [SerializeField]
    private GameObject hp4;
    [SerializeField]
    private GameObject hp5;
    void Start()
    {
        if (PlayerPrefs.GetInt("maxhealth") == 4)
        {
            hp4.gameObject.SetActive(true);
        }
        else if (PlayerPrefs.GetInt("maxhealth") == 5)
        {
            hp4.gameObject.SetActive(true);
            hp5.gameObject.SetActive(true);
        }
    }
}
